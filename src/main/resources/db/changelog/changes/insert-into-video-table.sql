INSERT INTO video (title, video_type_id, genre) VALUES
(
    'Transformers',
    (SELECT id FROM video_type WHERE type = 'REGULAR'),
    'ACTION'
);
INSERT INTO video (title, video_type_id, genre) VALUES
(
    'Despicable Me',
    (SELECT id FROM video_type WHERE type = 'CHILDREN' AND maximum_age = 16),
    'COMEDY'
);
INSERT INTO video (title, video_type_id, genre) VALUES
(
     'Strain',
     (SELECT id FROM video_type WHERE type = 'NEW_RELEASE' AND year_released = 2020),
     'DRAMA'
);
INSERT INTO video (title, video_type_id, genre) VALUES
(
    'Hitch',
    (SELECT id FROM video_type WHERE type = 'REGULAR'),
    'ROMANCE'
);
INSERT INTO video (title, video_type_id, genre) VALUES
(
    'Us',
    (SELECT id FROM video_type WHERE type = 'NEW_RELEASE' AND year_released = 2021),
    'HORROR'
);