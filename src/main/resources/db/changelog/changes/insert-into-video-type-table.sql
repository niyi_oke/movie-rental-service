INSERT  INTO  video_type  (type, rate)  VALUES  ('REGULAR', 10);
INSERT  INTO  video_type  (type, rate, maximum_age)  VALUES  ('CHILDREN', 8, 16);
INSERT  INTO  video_type  (type, rate, year_released)  VALUES  ('NEW_RELEASE', 15, 2020);
INSERT  INTO  video_type  (type, rate, year_released)  VALUES  ('NEW_RELEASE', 15, 2021);