CREATE TABLE video_type (
  id  BIGSERIAL NOT NULL,
  type VARCHAR(255) NOT NULL,
  maximum_age INT,
  year_released INT,
  rate FLOAT,
  deleted BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (id)
);