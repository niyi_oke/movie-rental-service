CREATE TABLE rented_video (
  id  BIGSERIAL NOT NULL,
  username VARCHAR(255) NOT NULL,
  title VARCHAR(255) NOT NULL,
  number_of_days INT8 NOT NULL,
  price NUMERIC(19, 2) NOT NULL,
  deleted BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (id)
);
