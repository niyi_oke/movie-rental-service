CREATE TABLE video (
  id  BIGSERIAL NOT NULL,
  title VARCHAR(255) NOT NULL,
  video_type_id INT8 NOT NULL,
  genre VARCHAR(255) NOT NULL,
  deleted BOOLEAN DEFAULT FALSE,
  PRIMARY KEY (id)
);

create index idx_video_title on video (title);
