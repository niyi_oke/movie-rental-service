package com.niyioke.movierentalapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.niyioke.movierentalapi.entity.VideoType;
import com.niyioke.movierentalapi.enums.Genre;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoDTO {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long id;

    @NotNull(message = "Video title is required")
    private String title;

    @NotNull(message = "Video type is required")
    private VideoType videoType;

    @NotNull(message = "Video genre is required")
    private Genre genre;
}
