package com.niyioke.movierentalapi.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RentVideoRequestDTO {

    @NotNull(message = "user name is required")
    private String name;

    @NotNull(message = "Video title is required")
    private String title;

    @NotNull(message = "number of days is required")
    private Integer numberOfDays;
}
