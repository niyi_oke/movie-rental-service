package com.niyioke.movierentalapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "rented_video")
@Where(clause = "deleted = false")
public class RentedVideo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String title;

    @Column(name = "number_of_days")
    private Integer numberOfDays;

    private BigDecimal price;

    @JsonIgnore
    @Column(columnDefinition = "boolean default false")
    private boolean deleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof RentedVideo))
            return false;

        RentedVideo other = (RentedVideo) o;

        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
