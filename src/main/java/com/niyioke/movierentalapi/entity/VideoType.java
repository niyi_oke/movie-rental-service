package com.niyioke.movierentalapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.niyioke.movierentalapi.enums.Type;
import com.niyioke.movierentalapi.service.IVideoType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;


@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video_type")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type", discriminatorType = DiscriminatorType.STRING)
public class VideoType implements IVideoType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(insertable = false, updatable = false)
    private Type type;

    private Double rate;

    @JsonIgnore
    @Column(columnDefinition = "boolean default false")
    private boolean deleted;

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {
        return BigDecimal.valueOf(this.getRate()).multiply(BigDecimal.valueOf(numberOfDays));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof VideoType))
            return false;

        VideoType other = (VideoType) o;

        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
