package com.niyioke.movierentalapi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("NEW_RELEASE")
public class NewReleaseType extends VideoType {

    @Column(name = "year_released")
    @Pattern(regexp = "^\\d{4}$", message = "year released must be of format yyyy")
    private Integer yearReleased;

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {
        return super.calculatePrice(numberOfDays)
                .subtract(BigDecimal.valueOf(LocalDate.now().getYear() - this.getYearReleased()));
    }
}
