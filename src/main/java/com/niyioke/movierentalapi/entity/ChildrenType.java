package com.niyioke.movierentalapi.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("CHILDREN")
public class ChildrenType extends VideoType {

    @Column(name = "maximum_age")
    private Integer maximumAge;

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {
        return super.calculatePrice(numberOfDays).add(BigDecimal.valueOf(this.getMaximumAge()/ 2));
    }
}
