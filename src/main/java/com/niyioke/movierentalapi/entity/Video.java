package com.niyioke.movierentalapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.niyioke.movierentalapi.enums.Genre;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "video")
@Where(clause = "deleted = false")
public class Video {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Video title is required")
    private String title;

    @OneToOne(cascade = CascadeType.ALL)
    private VideoType videoType;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Video genre is required")
    private Genre genre;

    @JsonIgnore
    @Column(columnDefinition = "boolean default false")
    private boolean deleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Video))
            return false;

        Video other = (Video) o;

        return id != null && id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
