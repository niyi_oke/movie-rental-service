package com.niyioke.movierentalapi.exception;

import com.niyioke.movierentalapi.controller.VideoController;
import com.niyioke.movierentalapi.dto.ResponseObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.persistence.EntityNotFoundException;

/**
 * @author Oke
 */
@Slf4j
@RestControllerAdvice(basePackageClasses = VideoController.class)
public class GlobalExceptionHandler {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<ResponseObject> errorHandler(MethodArgumentNotValidException e) {
        log.error(null, e);
        ResponseObject response = ResponseObject.builder()
                .status(ResponseObject.ResponseStatus.FAILED)
                .message(e.getBindingResult().getFieldErrors().get(0).getDefaultMessage())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ResponseObject> errorHandler(Exception e) {
        log.error(e.getMessage(), e);
        ResponseObject response = ResponseObject.builder()
                .status(ResponseObject.ResponseStatus.FAILED)
                .message("An error has occurred while processing your request, please try again")
                .build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<ResponseObject> errorHandler(EntityNotFoundException e) {
        log.error(e.getMessage(), e);
        ResponseObject response = ResponseObject.builder()
                .status(ResponseObject.ResponseStatus.FAILED)
                .message(e.getMessage())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

}
