package com.niyioke.movierentalapi.controller;

import com.niyioke.movierentalapi.dto.RentVideoRequestDTO;
import com.niyioke.movierentalapi.dto.ResponseObject;
import com.niyioke.movierentalapi.service.VideoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/video")
public class VideoController {

    private final VideoService videoService;

    @GetMapping
    public ResponseObject getAllVideos(@PageableDefault Pageable pageable) {
        return ResponseObject
                .builder()
                .status(ResponseObject.ResponseStatus.SUCCESSFUL)
                .data(videoService.fetchAllVideos(pageable))
                .build();
    }

    @PostMapping("/price")
    public ResponseObject calculatePrice(@RequestBody @Valid RentVideoRequestDTO videoRequestDTO) {
        return ResponseObject
                .builder()
                .status(ResponseObject.ResponseStatus.SUCCESSFUL)
                .data(videoService.calculateRentalPrice(videoRequestDTO))
                .build();
    }
}
