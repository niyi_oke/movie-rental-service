package com.niyioke.movierentalapi.service;

import com.niyioke.movierentalapi.dto.RentVideoRequestDTO;
import com.niyioke.movierentalapi.dto.RentVideoResponseDTO;
import com.niyioke.movierentalapi.dto.VideoDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface VideoService {

    Page<VideoDTO> fetchAllVideos(Pageable pageable);

    RentVideoResponseDTO calculateRentalPrice(RentVideoRequestDTO rentVideoRequestDTO);
}
