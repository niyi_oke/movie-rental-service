package com.niyioke.movierentalapi.service.impl;

import com.niyioke.movierentalapi.dto.RentVideoRequestDTO;
import com.niyioke.movierentalapi.dto.RentVideoResponseDTO;
import com.niyioke.movierentalapi.dto.VideoDTO;
import com.niyioke.movierentalapi.entity.RentedVideo;
import com.niyioke.movierentalapi.entity.Video;
import com.niyioke.movierentalapi.repository.RentedVideoRepository;
import com.niyioke.movierentalapi.repository.VideoRepository;
import com.niyioke.movierentalapi.service.VideoService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;

@Service
@AllArgsConstructor
public class VideoServiceImpl implements VideoService {

    private final VideoRepository videoRepository;

    private final RentedVideoRepository rentedVideoRepository;

    @Override
    public Page<VideoDTO> fetchAllVideos(Pageable pageable) {
        return videoRepository.findAll(pageable)
                .map(video -> VideoDTO.builder()
                        .id(video.getId())
                        .title(video.getTitle())
                        .videoType(video.getVideoType())
                        .genre(video.getGenre())
                        .build());
    }

    @Override
    public RentVideoResponseDTO calculateRentalPrice(RentVideoRequestDTO rentVideoRequestDTO) {
        final Video video = videoRepository.findByTitle(rentVideoRequestDTO.getTitle())
                .orElseThrow(() -> new EntityNotFoundException("video not found"));

        final BigDecimal price = video.getVideoType().calculatePrice(rentVideoRequestDTO.getNumberOfDays());

        rentedVideoRepository.save(RentedVideo.builder()
                .username(rentVideoRequestDTO.getName())
                .title(rentVideoRequestDTO.getTitle())
                .numberOfDays(rentVideoRequestDTO.getNumberOfDays())
                .price(price)
                .build());

        return new RentVideoResponseDTO(price);
    }
}
