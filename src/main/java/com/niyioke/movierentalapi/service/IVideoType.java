package com.niyioke.movierentalapi.service;

import java.math.BigDecimal;

public interface IVideoType {

    BigDecimal calculatePrice(Integer numberOfDays);
}
