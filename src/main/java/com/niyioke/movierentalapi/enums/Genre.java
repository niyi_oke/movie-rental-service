package com.niyioke.movierentalapi.enums;

public enum Genre {
    ACTION, DRAMA, ROMANCE, COMEDY, HORROR;
}
