package com.niyioke.movierentalapi.enums;

public enum Type {

    REGULAR, CHILDREN, NEW_RELEASE
}
