package com.niyioke.movierentalapi.repository;

import com.niyioke.movierentalapi.entity.Video;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VideoRepository extends JpaRepository<Video, Long> {

    Optional<Video> findByTitle(String title);
}
