package com.niyioke.movierentalapi.repository;

import com.niyioke.movierentalapi.entity.RentedVideo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentedVideoRepository extends JpaRepository<RentedVideo, Long> {
}
