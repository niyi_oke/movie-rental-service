package com.niyioke.movierentalapi.repository;

import com.niyioke.movierentalapi.entity.Video;
import com.niyioke.movierentalapi.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class VideoRepositoryTest {

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void fetchAllVideos() {

        Page<Video> videoPage = videoRepository.findAll(Pageable.unpaged());

        assertThat(videoPage.getTotalElements())
                .isEqualTo(5);
        assertThat(videoPage.getTotalPages())
                .isEqualTo(1);
        assertThat(videoPage.getContent().get(0))
                .isEqualTo(TestUtil.getVideo());
    }

    @Test
    void findByTitle() {

        Video result = videoRepository.findByTitle("Transformers")
                .orElseThrow(() -> new EntityNotFoundException("video not found"));

        assertThat(result)
                .isEqualTo(TestUtil.getVideo());
    }
}