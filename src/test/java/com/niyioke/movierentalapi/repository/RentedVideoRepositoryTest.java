package com.niyioke.movierentalapi.repository;

import com.niyioke.movierentalapi.entity.RentedVideo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
@ExtendWith(SpringExtension.class)
class RentedVideoRepositoryTest {

    @Autowired
    private RentedVideoRepository rentedVideoRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    void save() {

        final RentedVideo result = rentedVideoRepository.save(RentedVideo.builder()
                .username("My name")
                .title("Transformers")
                .numberOfDays(7)
                .price(BigDecimal.TEN)
                .build());

        assertThat(result)
                .isEqualTo(testEntityManager.find(RentedVideo.class, result.getId()));
    }
}