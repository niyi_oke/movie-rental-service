package com.niyioke.movierentalapi.service.impl;

import com.niyioke.movierentalapi.dto.RentVideoResponseDTO;
import com.niyioke.movierentalapi.dto.VideoDTO;
import com.niyioke.movierentalapi.repository.RentedVideoRepository;
import com.niyioke.movierentalapi.repository.VideoRepository;
import com.niyioke.movierentalapi.service.VideoService;
import com.niyioke.movierentalapi.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = VideoServiceImpl.class)
class VideoServiceImplTest {

    @MockBean
    private VideoRepository videoRepository;

    @MockBean
    private RentedVideoRepository rentedVideoRepository;

    @Autowired
    private VideoService videoService;

    @Test
    void fetchAllVideos() {

        when(videoRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(Collections.singletonList(TestUtil.getVideo())));

        Page<VideoDTO> result = videoService.fetchAllVideos(Pageable.unpaged());

        assertThat(result.getTotalElements())
                .isEqualTo(1);
        assertThat(result.getTotalPages())
                .isEqualTo(1);
        assertThat(result.getContent().get(0))
                .isEqualTo(TestUtil.getVideoDTO());
    }

    @Test
    void calculateRentalPrice() {

        when(videoRepository.findByTitle(anyString()))
                .thenReturn(Optional.of(TestUtil.getVideo()));

        RentVideoResponseDTO result = videoService.calculateRentalPrice(TestUtil.getRentVideoRequestDTO());

        assertThat(result.getPrice())
                .isEqualTo(BigDecimal.valueOf(70.0));
    }
}