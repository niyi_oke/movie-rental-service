package com.niyioke.movierentalapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.niyioke.movierentalapi.dto.RentVideoRequestDTO;
import com.niyioke.movierentalapi.dto.RentVideoResponseDTO;
import com.niyioke.movierentalapi.service.VideoService;
import com.niyioke.movierentalapi.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VideoController.class)
class VideoControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    private VideoService videoService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllVideos() throws Exception {

        when(videoService.fetchAllVideos(any()))
                .thenReturn(new PageImpl<>(Collections.singletonList(TestUtil.getVideoDTO())));

        mockMvc.perform(get("/api/v1/video")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("Successful"))
                .andExpect(jsonPath("$.data.content.[0].id").value(1))
                .andExpect(jsonPath("$.data.content.[0].title").value("Transformers"))
                .andExpect(jsonPath("$.data.content.[0].videoType.type").value("REGULAR"))
                .andExpect(jsonPath("$.data.totalPages").value(1))
                .andExpect(jsonPath("$.data.totalElements").value(1))
                .andExpect(jsonPath("$.data.size").value(1))
                .andExpect(jsonPath("$.data.number").value(0))
                .andExpect(jsonPath("$.data.numberOfElements").value(1));
    }

    @Test
    void calculatePrice() throws Exception {

        when(videoService.calculateRentalPrice(any(RentVideoRequestDTO.class)))
                .thenReturn(new RentVideoResponseDTO(BigDecimal.TEN));

        mockMvc.perform(post("/api/v1/video/price")
                .content(objectMapper.writeValueAsString(TestUtil.getRentVideoRequestDTO()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("Successful"))
                .andExpect(jsonPath("$.data.price").value(10));

    }
}