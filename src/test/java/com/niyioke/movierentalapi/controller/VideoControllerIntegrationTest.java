package com.niyioke.movierentalapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.niyioke.movierentalapi.util.TestUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class VideoControllerIntegrationTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllVideos() throws Exception {

        mockMvc.perform(get("/api/v1/video")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("Successful"))
                .andExpect(jsonPath("$.data.content.[0].id").value(1))
                .andExpect(jsonPath("$.data.content.[0].title").value("Transformers"))
                .andExpect(jsonPath("$.data.content.[0].videoType.type").value("REGULAR"))
                .andExpect(jsonPath("$.data.totalPages").value(1))
                .andExpect(jsonPath("$.data.totalElements").value(5))
                .andExpect(jsonPath("$.data.size").value(10))
                .andExpect(jsonPath("$.data.number").value(0))
                .andExpect(jsonPath("$.data.numberOfElements").value(5));
    }

    @Test
    void calculatePrice() throws Exception {

        mockMvc.perform(post("/api/v1/video/price")
                .content(objectMapper.writeValueAsString(TestUtil.getRentVideoRequestDTO()))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("Successful"))
                .andExpect(jsonPath("$.data.price").value(70));

    }
}
