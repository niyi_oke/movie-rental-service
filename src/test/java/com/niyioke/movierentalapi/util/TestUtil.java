package com.niyioke.movierentalapi.util;

import com.niyioke.movierentalapi.dto.RentVideoRequestDTO;
import com.niyioke.movierentalapi.dto.VideoDTO;
import com.niyioke.movierentalapi.entity.RegularType;
import com.niyioke.movierentalapi.entity.Video;
import com.niyioke.movierentalapi.entity.VideoType;
import com.niyioke.movierentalapi.enums.Genre;
import com.niyioke.movierentalapi.enums.Type;

public class TestUtil {

    public static VideoType getRegularType(){
        RegularType regularType = new RegularType();
        regularType.setType(Type.REGULAR);
        regularType.setId(1L);
        regularType.setRate(10.0);
        regularType.setDeleted(false);
        return regularType;
    }

    public static Video getVideo() {
        return Video.builder()
                .id(1L)
                .title("Transformers")
                .videoType(getRegularType())
                .genre(Genre.ACTION)
                .build();
    }

    public static VideoDTO getVideoDTO() {
        return VideoDTO.builder()
                .id(1L)
                .title("Transformers")
                .videoType(getRegularType())
                .genre(Genre.ACTION)
                .build();
    }

    public static RentVideoRequestDTO getRentVideoRequestDTO(){
        return RentVideoRequestDTO.builder()
                .name("My name")
                .title("Transformers")
                .numberOfDays(7)
                .build();
    }
}
